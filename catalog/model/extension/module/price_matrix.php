<?php

class ModelExtensionModulePriceMatrix extends Model
{
    public $module_settings;

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->module_settings = $this->config->get('module_price_matrix');
    }

    /**
     * Returns true if module is enabled and configured correctly
     * @return bool $is_module_configured
     */
    public function isConfigured()
    {
        $settings = $this->module_settings;
        if (!$settings) {
            return false;
        }
        if (!$settings['module_price_matrix_status']) {
            return false;
        }
        if (!$settings['x_axis_option_id'] || !$settings['y_axis_option_id']) {
            return false;
        }
        return true;
    }

    public function getXOptionId()
    {
        return $this->module_settings['x_axis_option_id'];
    }

    public function getYOptionId()
    {
        return $this->module_settings['y_axis_option_id'];
    }
}
