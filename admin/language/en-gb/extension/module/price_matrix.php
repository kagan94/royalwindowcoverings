<?php
$_['heading_title'] = '<b>Price Matrix</b>';

$_['success_msg'] = 'Module settings were saved successfully!';
$_['modules'] = 'Modules';
$_['text_edit_module_settings'] = 'Edit module settings';
$_['general_settings'] = 'General settings';
$_['support'] = 'Support';
$_['save_settings'] = 'Save settings';

$_['entry_option_sort_order'] = 'Option sort order';
$_['entry_horizontal_option'] = 'Option by horizontal (axis X)';
$_['entry_vertical_option'] = 'Option by vertical (axis Y)';

$_['error_permission'] = 'Error: You do not have enough rights to edit the module settings!';
