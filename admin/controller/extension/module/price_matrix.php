<?php

/*
 *******************************************************************************
 *  Module: Price Matrix
 *
 *  Web-site: http://opencart-modules.com
 *  Email: dev.dashko@gmail.com
 *  © Leonid Dashko
 *
 *  Below source-code or any part of the source-code cannot be resold or distributed.
 ******************************************************************************
 */

class ControllerModulePriceMatrix extends Controller
{
    private $errors = array();
    private $module_name = 'module/price_matrix';
    private $module_model;

    public function __construct($registry)
    {
        parent::__construct($registry);

        if (version_compare(VERSION, '2.3.0.0', '>=')) {
            $this->module_name = 'extension/module/price_matrix';
        }

        $this->load->model('setting/setting');
        $this->load->model('catalog/option');

        $this->load->model('extension/module/price_matrix');
        $this->module_model = $this->model_extension_module_price_matrix;

        $this->_loadLang();
    }

    public function install()
    {
        $this->module_model->install();
    }

    public function uninstall()
    {
        $this->module_model->uninstall();
    }

    public function index()
    {
        $data = $this->_loadLang();
        $price_matrix_option = $this->module_model->getPriceMatrixOption();

        $this->document->setTitle(str_replace(array("<b>", "</b>"), "", $this->language->get('heading_title')));

        $input = $this->request->post;
        if ($this->isPostRequest() && $this->validate($input)) {
            $price_matrix_option_id = $price_matrix_option['option_id'];

            // Update chosen options' names
            if ($input['x_axis_option_id'] && $input['y_axis_option_id']) {
                $this->load->model('catalog/option');
                $x_axis_option_descriptions = $this->model_catalog_option->getOptionDescriptions($input['x_axis_option_id']);
                $input['x_axis_option_names'] = $this->arrayColumnPreserveIndexes($x_axis_option_descriptions, 'name');
                $y_axis_option_descriptions = $this->model_catalog_option->getOptionDescriptions($input['y_axis_option_id']);
                $input['y_axis_option_names'] = $this->arrayColumnPreserveIndexes($y_axis_option_descriptions, 'name');
            }

            $input['price_matrix_option_id'] = $price_matrix_option_id;
            $input['module_price_matrix'] = $input;
            $this->model_setting_setting->editSetting('module_price_matrix', $input);

            $this->module_model->updatePriceMatrixSortOrder($input, $price_matrix_option_id);

            $this->session->data['success'] = $this->language->get('success_msg');
            $this->response->redirect($this->link($this->module_name, '', 'SSL'));
        }

        $module_settings = $this->config->get('module_price_matrix');
        if ($module_settings) {
            $data = array_merge($data, $module_settings);
        }

        # Show Success msgs
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        # Show Errors msgs
        if (!empty($this->errors)) {
            $this->session->data['errors'] = $this->errors;
        }

        if (isset($this->session->data['errors'])) {
            $data['errors'] = $this->session->data['errors'];
            unset($this->session->data['errors']);
        } else {
            $data['errors'] = '';
        }

        $options = $this->model_catalog_option->getOptions();
        $data['select_options'] = array_filter($options, function (&$option) {
            return $option['type'] == 'select';
        });

        # Breadcrumbs
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->link('common/dashboard', '', 'SSL'),
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('modules'),
            'href' => $this->getModulesLink()
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->link($this->module_name, '', 'SSL'),
        );

        # Actions
        $data['action'] = $this->link($this->module_name, '', 'SSL');
        $data['cancel'] = $this->getModulesLink();
        $data['token_get_param'] = $this->getTokenGetParam();
        $data['link_to_support'] = 'http://opencart-modules.com/tab-modules?lang=' . trim($this->config->get('config_admin_language'));

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->renderPage($data);
    }

    protected function renderPage(&$data)
    {
        $template_path = $this->module_name;
        if (version_compare(VERSION, '3.0.0.0', '<')) {
            $template_path .= '.tpl';
        }
        $this->response->setOutput($this->load->view($template_path, $data));
    }

    protected function link($route, $args = '', $secure = false)
    {
        if (version_compare(VERSION, '2.3.0.0', '>=')) {
            return $this->url->link($route, $args . '&user_token=' . $this->session->data['user_token'], $secure);
        }
        return $this->url->link($route, $args . '&token=' . $this->session->data['token'], $secure);
    }

    protected function getTokenGetParam()
    {
        if (version_compare(VERSION, '2.3.0.0', '>')) {
            return 'user_token=' . $this->session->data['user_token'];
        }
        return 'token=' . $this->session->data['token'];
    }

    protected function getModulesLink()
    {
        if (version_compare(VERSION, '2.3.0.0', '<')) {
            return $this->link('extension/module', '', 'SSL');
        } else if (version_compare(VERSION, '3.0.0.0', '>=')) {
            return $this->link('marketplace/extension', 'type=module', 'SSL');
        }
        return $this->link('extension/extension', 'type=module', 'SSL');
    }

    private function isPostRequest()
    {
        return $this->request->server['REQUEST_METHOD'] == 'POST';
    }

    private function validate(&$input)
    {
        if (!$this->user->hasPermission('modify', $this->module_name)) {
            $this->session->data['errors'][] = $this->language->get('error_permission');
            return false;
        }
        return true;
    }

    private function _loadLang(&$data = array())
    {
        $this->load->language('catalog/product');
        $this->load->language($this->module_name);

        $data['l'] = $this->language;

        return $data;
    }

    /* Helpers ================================= */
    public function arrayColumnPreserveIndexes(&$array, $column_name) {
        $result = array();
        foreach ($array as $key => $value) {
            $result[$key] = $value[$column_name];
        }
        return $result;
    }
}

# Compatibility with OC >= 2.3.x
class ControllerExtensionModulePriceMatrix extends ControllerModulePriceMatrix
{
}
