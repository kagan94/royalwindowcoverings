<?php

class ModelExtensionModulePriceMatrix extends Model
{
    public $lang_id;
    public $module_settings;

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->lang_id = (int)$this->config->get('config_language_id');
        $this->module_settings = $this->config->get('module_price_matrix');
    }

    public function install()
    {
        $this->load->model('localisation/language');
        $langs = $this->model_localisation_language->getLanguages();

        if (!$this->columnExists('product_option_value', 'dependent_on_option_id'))
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD dependent_on_option_id int(11) DEFAULT NULL ");

        if (!$this->columnExists('product_option_value', 'dependent_on_option_value_id'))
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD dependent_on_option_value_id int(11) DEFAULT NULL ");

        $price_matrix_option = $this->getPriceMatrixOption();
        if (!$price_matrix_option) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET `type` = 'price_matrix'");
            $price_matrix_option_id = $this->db->getLastId();

            foreach ($langs as $lang) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "option_description` SET `option_id` = '" . $price_matrix_option_id . "', `language_id` = '" . $lang['language_id'] . "', `name` = 'Price matrix'");
            }
        }
    }

    public function uninstall()
    {
        if ($this->columnExists('product_option_value', 'dependent_on_option_id'))
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value DROP COLUMN dependent_on_option_id");

        if ($this->columnExists('product_option_value', 'dependent_on_option_value_id'))
            $this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value DROP COLUMN dependent_on_option_value_id");

        $price_matrix_option = $this->getPriceMatrixOption();
        if ($price_matrix_option) {
            $this->db->query("DELETE FROM `" . DB_PREFIX . "option` WHERE `option_id` = '" . $price_matrix_option['option_id'] . "'");
            $this->db->query("DELETE FROM `" . DB_PREFIX . "option_description` WHERE `option_id` = '" . $price_matrix_option['option_id'] . "'");
        }
    }

    private function columnExists($table, $column_name)
    {
        $column_dependent_on_option_id_exists = $this->db->query("
          SELECT column_name
          FROM information_schema.columns
          WHERE table_schema='" . DB_DATABASE . "' AND table_name='" . DB_PREFIX . $table . "' AND column_name='" . $column_name . "'
          ")->rows;
        return isset($column_dependent_on_option_id_exists[0]['column_name']);
    }

    public function getPriceMatrixOption()
    {
        return $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` WHERE `type` = 'price_matrix'")->row;
    }

    public function updatePriceMatrixSortOrder($input, $price_matrix_option_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "option SET sort_order = '" . (int)$input['sort_order'] . "' WHERE option_id = '" . $price_matrix_option_id . "'");
    }

    /* =============================================================== */
    /* Helpers ======================================================= */
    /* =============================================================== */

    /**
     * Returns true if module is enabled and configured correctly
     * @return bool $is_module_configured
     */
    public function isConfigured()
    {
        $settings = $this->module_settings;
        if (!$settings) {
            return false;
        }
        if (!$settings['module_price_matrix_status']) {
            return false;
        }
        if (!$settings['x_axis_option_id'] || !$settings['y_axis_option_id']) {
            return false;
        }
        return true;
    }

    public function getXAxisName() {
        return $this->module_settings['x_axis_option_names'][$this->lang_id];
    }

    public function getYAxisName() {
        return $this->module_settings['y_axis_option_names'][$this->lang_id];
    }

    public function getXOptionId() {
        return $this->module_settings['x_axis_option_id'];
    }

    public function getYOptionId() {
        return $this->module_settings['y_axis_option_id'];
    }

    public function getPriceMatrixOptionId() {
        return $this->module_settings['price_matrix_option_id'];
    }
}
